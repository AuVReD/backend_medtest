import logging

from flask import request
from flask_restplus import Resource
from tests_backend.api.serializers import themes
from tests_backend.api.restplus import api
from tests_backend.models import Question, Theme
from flask_mail import Message
from flask import current_app
from tests_backend.extensions import mail

log = logging.getLogger(__name__)

questions_route = api.namespace('questions', description='themes operations')


@questions_route.route('/<int:question_id>')
@api.response(404, 'Question not found.')
class QuestionItem(Resource):

    def get(self, question_id):
        question = Question.query.filter(Question.id == question_id).one()
        return question.as_dict()


@questions_route.route('/abuse')
class QuestionItemAbuse(Resource):

    def post(self):
        import smtplib

        theme_title = request.json['theme']
        question_title = request.json['question']
        url = request.json['url']
        message = request.json['message']
        user_email = request.json['user_email']

        fromx = current_app.config['MAIL_USERNAME']
        to = 'reanimatolog@bk.ru'
        body = 'Тема: {0}\n\nВопрос: {1}\n\nURL: {2}\n\nСообщение: {3}\n\nUser Email: {4}'. \
            format(theme_title, question_title, url, message, user_email)

        subject = 'Отзыв о тестовом контроле'  # Line that causes trouble

        mail_details = """From: %s\nTo:s %s\nSubject: %s\n\n%s
            """ % (fromx, to, subject, body)

        server = smtplib.SMTP(current_app.config['MAIL_SERVER'], current_app.config['MAIL_PORT'])
        server.starttls()
        server.ehlo()
        server.login(current_app.config['MAIL_USERNAME'], current_app.config['MAIL_PASSWORD'])
        server.sendmail(fromx, to, mail_details.encode('utf-8'))
        server.quit()

        return 200